package Day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

public class Demo1 {
    public static void main(String[] args) {

        Connection con = DbConnection.getConnection();
        PreparedStatement pst = null;
        ResultSet rs = null;

        String selectQuery = "SELECT * FROM student";

        try {

            pst = con.prepareStatement(selectQuery);
            rs = pst.executeQuery();

            while (rs.next()) {
                System.out.print(rs.getInt("student_id") + " ");
                System.out.print(rs.getString("student_name") + " ");
                System.out.print(rs.getDouble("gpa") + " ");
                System.out.println(rs.getString("major") + " " + rs.getString("enrollment_date"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    rs.close();
                    pst.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
