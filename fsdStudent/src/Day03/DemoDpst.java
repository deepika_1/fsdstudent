package Day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnector;
public class DemoDpst {
	public static void main(String[] args) {
		
		Connection con = DbConnector.getConnector();
		PreparedStatement pst = null;
	
		System.out.print("Enter Student Id: ");
		int stuId = new Scanner(System.in).nextInt();
		System.out.println();
		
		String deleteQuery = "delete from student where stuId = (?)";		
		
		try {
			
			pst = con.prepareStatement(deleteQuery);
			pst.setInt(1, stuId);
			
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Student Record Deleted!!!");
			} else {
				System.out.println("Failed to delete the Student Record!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}
