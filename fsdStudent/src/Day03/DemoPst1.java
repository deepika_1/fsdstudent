package Day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnector;

//Inserting a record
public class DemoPst1 {
	public static void main(String[] args) {
		
		Connection con = DbConnector.getConnector();
		PreparedStatement pst = null;
		
		System.out.println("Enter student Id, Name, age, Gender,emailId");
		Scanner scan = new Scanner(System.in);
		int stuId = Integer.parseInt( scan.nextLine());
		String stuName = scan.nextLine();
		int age = Integer.parseInt(scan.nextLine());
		String gender = scan.nextLine();
		String emailId=scan.nextLine();
		
		System.out.println();
		
		String insertQuery = "insert into student values (?, ?, ?, ?)";
		
		try {
			
			pst = con.prepareStatement(insertQuery);			
			pst.setInt(1,stuId);
			pst.setString(2, stuName);
			pst.setDouble(3, age);
			pst.setString(4, gender);
			pst.setString(5, emailId);
						
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Inserted!!!");
			} else {
				System.out.println("Failed to Insert the Record.");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}

