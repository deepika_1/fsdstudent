package Day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnector;

public class UpdateStudent {
	public static void main(String[] args) {
		
		Connection con = DbConnector.getConnector();

		PreparedStatement pst = null;
	
		System.out.println("Enter Student Id and age");
		Scanner scan = new Scanner(System.in);
		int stuId = scan.nextInt();;
		double age = scan.nextDouble();
		System.out.println();
		
		String updateQuery = "update student set age = (?) where stuId = (?) " ;
		
		try {
			
			pst = con.prepareStatement(updateQuery);
			pst.setInt(2, stuId);
			pst.setDouble(1,age);
			int result = pst.executeUpdate();
			
			if (result > 0) {
				System.out.println("Student Record Updated!!!");
			} else {
				System.out.println("Failed to Update the Student Record!!!");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}		
	}
}
