package day01;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.sql.Statement;

//Fetch Employee Data: select * from employee
public class Demo3Delete {
	public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		Object rs = null;
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "delete from student where studentId=103 ";
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");
			
			stmt = con.createStatement();
			rs = stmt.executeUpdate(query);
			
			if(rs != null){
				System.out.println("Record deleted successfully");
			}
			
			
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}