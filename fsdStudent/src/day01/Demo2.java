package day01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Fetch Employee Data: select * from employee with empId 101
public class Demo2 {
	public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "Select * from student where studentId=101;";
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");
			
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			
			rs.next();
			System.out.println("StudentId   : " + rs.getInt(1));
			System.out.println("StudentName : " + rs.getString("StudentName"));
			;
			System.out.println("Gender  : " + rs.getString("gender"));
			System.out.println("EmailId : " + rs.getString(4));
			System.out.println("Password: " + rs.getString(5) + "\n");
			
			
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}