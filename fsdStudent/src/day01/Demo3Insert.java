package day01;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.sql.Statement;

//Fetch Employee Data: select * from employee
public class Demo3Insert {
	public static void main(String[] args) {
		
		Connection con = null;
		Statement stmt = null;
		Object rs = null;
		
		String url = "jdbc:mysql://localhost:3306/fsd57";
		String query = "insert into student values (104, 'kavya', 'Female', 'kavya@gmail.com', '123');";
		
		try {
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(url, "root", "root");
			
			stmt = con.createStatement();
			rs = stmt.executeUpdate(query);
			
			if(rs != null){
				System.out.println("Record inserted successfully");
			}
			
			
			
			
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		
	}
}