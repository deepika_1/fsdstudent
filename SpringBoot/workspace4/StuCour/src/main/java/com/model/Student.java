package com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Student {

	@Id@GeneratedValue
	private int stuId;
	private String stuName;
	private double fee;
	private String gender;
	private Date doj;
	private String state;
	private String emailId;
	private String password;
	
	//Implementing Mapping Between Student and Course
	@ManyToOne
	@JoinColumn(name="courId")
	Course course;
	
	public Student() {
	}
	
	//Parameterized Constructor without stuId
	public Student (String stuName, double fee, String gender, Date doj, String state, String emailId,
			String password) {
		this.stuName = stuName;
		this.fee = fee;
		this.gender = gender;
		this.doj = doj;
		this.state = state;
		this.emailId = emailId;
		this.password = password;
	}

	public Student(int stuId, String stuName, double fee, String gender, Date doj, String state, String emailId,
			String password) {
		this.stuId = stuId;
		this.stuName = stuName;
		this.fee = fee;
		this.gender = gender;
		this.doj = doj;
		this.state = state;
		this.emailId = emailId;
		this.password = password;
	}
		
	//Generating Getter for course Variable
	public Course getCourse() {
		return course;
	}
	
	//Generating Setter for course Variable
	public void setCourse(Course course) {
		this.course = course;
	}

	public int getStuId() {
		return stuId;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

