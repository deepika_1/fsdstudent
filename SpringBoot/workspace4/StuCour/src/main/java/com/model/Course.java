package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {
	
	@Id
	private int courId;			
	private String courName;	
	private String location;
	
	//Implementing Mapping Between Department and Employee
	@JsonIgnore	//UnComment this later on
	@OneToMany(mappedBy="course")
	List<Student> stuList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int courId, String courName, String location) {
		this.courId = courId;
		this.courName = courName;
		this.location = location;
	}
	
	//Generating Getter for stuList Variable
	public List<Student> getStuList() {
		return stuList;
	}

	//Generating Setter for stuList Variable
	public void setStuList(List<Student> stuList) {
		this.stuList = stuList;
	}

	public int getCourId() {
		return courId;
	}

	public void setCourId(int courId) {
		this.courId = courId;
	}

	public String getCourName() {
		return courName;
	}

	public void setCourName(String courName) {
		this.courName = courName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
