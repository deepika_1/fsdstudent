package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

	@Query("from Student where stuName = :studentName")
	Student findByName(@Param("studentName") String studentName);

	@Query("from Student where emailId = :emailId and password = :password")
	Student studentLogin(@Param("emailId") String emailId, @Param("password") String password);
	
	@Query("from Student where emailId = :emailId")
	Student findByEmailId(@Param("emailId") String emailId);

}

