package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public Student getStudentById(int studentId) {
        return studentRepository.findById(studentId).orElse(null);
    }

    public Student getStudentByName(String studentName) {
        return studentRepository.findByName(studentName);
    }

    public Student studentLogin(String emailId, String password) {
        Student student = studentRepository.findByEmailId(emailId);

        if (student != null) {
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            if (bcrypt.matches(password, student.getPassword())) {
                return student;
            }
        }

        return null;
    }

    public Student addStudent(Student student) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(student.getPassword());
        student.setPassword(encryptedPwd);

        return studentRepository.save(student);
    }

    public Student updateStudent(Student student) {
        return studentRepository.save(student);
    }

    public void deleteStudentById(int studentId) {
        studentRepository.deleteById(studentId);
    }
}
