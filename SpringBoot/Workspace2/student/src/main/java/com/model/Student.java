package com.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	
	//Entity : Making Product Class as a Product Table under Database
	//Id     : Making prodId Column as a Primary Key Column
	//Column : Providing our own column name instead of variable name as the column name
	//GeneratedValue: Auto_Increment
	
	@Id@GeneratedValue
	private int stdId;			
	
	@Column(name="stdname")
	private String stdName;	
	private double fee;
	private String course;	
	public Student() {
		super();
	}

	public Student(int stdId, String stdName, double fee, String course) {
		super();
		this.stdId = stdId;
		this.stdName = stdName;
		this.fee = fee;
		this.course = course;
	}

	public int getstdId() {
		return stdId;
	}
	
	public void setstdId(int stdId) {
		this.stdId = stdId;
	}

	public String getstdName() {
		return stdName;
	}
	
	public void setstdName(String stdName) {
		this.stdName = stdName;
	}

	public double getfee() {
		return fee;
	}
	
	public void setfee(double fee) {
		this.fee = fee;
	}
	public String getcourse() {
		return course;
	}
	
	public void setcourse(String course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "Student [stdId=" + stdId + ", stdName=" + stdName + ", fee=" + fee + ", course="+ course +"]";
	}	
}