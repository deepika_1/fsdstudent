package com.ts.student;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	
        //Implementing Dependency Injection for ProductDao 
	@Autowired
	StudentDao studentDao;
	
	@GetMapping("getAllStudents")
	public List<Student> getAllstudent() {		
		return studentDao.getAllStudents();
	}
	@GetMapping("getStudentById/{studentId}")
	public Student getStudentById(@PathVariable("studentId") int studentId) {
		return studentDao.getStudentById(studentId);	
	}
	
}