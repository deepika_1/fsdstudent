package com.dao;



import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {
	
	//Implementing Dependency Injection for studenttRepository
	@Autowired
	StudentRepository studentRepository;

	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	public Student getStudentById(int studentId) {
		Student student = new Student(0, "Student Not Found!!!", 0.0, null);
		return studentRepository.findById(studentId).orElse(student);
	}
	}
	
	
	

	
