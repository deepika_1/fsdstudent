package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Course {
	
	//Entity : Making Product Class as a Product Table under Database
	//Id     : Making prodId Column as a Primary Key Column
	//Column : Providing our own column name instead of variable name as the column name
	//GeneratedValue: Auto_Increment
	
	@Id@GeneratedValue
	private int courId;			
	
	@Column(name="cname")
	private String courName;	
	private double price;
		
	public Course() {
		super();
	}

	public Course(int courId, String courName, double price) {
		super();
		this.courId = courId;
		this.courName = courName;
		this.price = price;
	}

	public int getCourId() {
		return courId;
	}
	
	public void setCourId(int courId) {
		this.courId = courId;
	}

	public String getCourName() {
		return courName;
	}
	
	public void setCourName(String courName) {
		this.courName = courName;
	}

	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Course [courId=" + courId + ", courName=" + courName + ", price=" + price + "]";
	}	
}

