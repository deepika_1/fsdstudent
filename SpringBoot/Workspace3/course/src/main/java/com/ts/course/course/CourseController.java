package com.ts.course.course;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDao;
import com.model.Course;

@RestController
public class CourseController {
	
	@Autowired
	CourseDao courseDao;
	
	@GetMapping("getAllCourses")
	public List<Course> getAllCourses() {		
		return courseDao.getAllCourses();
	}
	
	@GetMapping("getCourseById/{courseId}")
	public Course getCourseById(@PathVariable("courseId") int courseId) {
		return courseDao.getCourseById(courseId);	
	}
	
	@GetMapping("getCourseByName/{courseName}")
	public List<Course> getCourseByName(@PathVariable("courseName") String courseName) {
		return courseDao.getCourseByName(courseName);
	}
	
	@PostMapping("addCourse")
	public Course addCourse(@RequestBody Course course) {
		return courseDao.addCourse(course);
	}
	
	@PutMapping("updateCourseById/{courseId}")
    public Course updateCourseById(@PathVariable("courseId") int courseId, @RequestBody Course updatedCourse) {
        return courseDao.updateCourseById(courseId, updatedCourse);
    }
	@DeleteMapping("deleteCourseById/{courseid}")
	public String deleteCourseById(@PathVariable("courseid") int courId) {
		courseDao.deleteCourseById(courId);
		return "Course with CourseId: " + courId + ", Deleted Successfully";
	}



}

