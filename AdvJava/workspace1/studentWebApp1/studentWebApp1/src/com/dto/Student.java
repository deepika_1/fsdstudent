package com.dto;

public class Student {
	private int studentId;
	private String studentName;
	
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int studentId, String studentName,  String gender, String emailId, String password) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	
	

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ",  gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}


