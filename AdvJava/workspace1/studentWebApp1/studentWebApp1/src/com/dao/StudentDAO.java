package com.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;

import com.dto.Student;


public class StudentDAO {
public Student studentLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student where emailId=? and password=?";
		
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student emp = new Student();
				emp.setStudentId(rs.getInt(1));
				emp.setStudentName(rs.getString(2));
				
				emp.setGender(rs.getString(3));
				emp.setEmailId(rs.getString(4));
				emp.setPassword(rs.getString(5));
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return null;
	}
public List<Student> getAllStudents() {
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;		
	List<Student> empList = null;
	
	String selectQuery = "Select * from student";
	
	
	try {
		pst = con.prepareStatement(selectQuery);
		rs = pst.executeQuery();
		
		empList = new ArrayList<Student>();
		
		while (rs.next()) {
			Student emp = new Student();
			
			emp.setStudentId(rs.getInt(1));
			emp.setStudentName(rs.getString(2));
			
			emp.setGender(rs.getString(3));
			emp.setEmailId(rs.getString(4));
			emp.setPassword(rs.getString(5));
			
			empList.add(emp);
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return empList;
	
}
public int registerStudent(Student emp) {
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String insertQuery = "insert into student " + 
	"(studentId,studentName, gender, emailId, password) values ( ?,?, ?, ?, ?)";
	
	try {
		pst = con.prepareStatement(insertQuery);
		pst.setInt(1, emp.getStudentId());
		
		pst.setString(2, emp.getStudentName());
		
		pst.setString(3, emp.getGender());
		pst.setString(4, emp.getEmailId());
		pst.setString(5, emp.getPassword());
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return 0;
}
public Student getStudentById(int studentId) {	
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	String selectQuery = "Select * from student where studentId=?";
	
	
	try {
		pst = con.prepareStatement(selectQuery);
		pst.setInt(1, studentId);
		rs = pst.executeQuery();
		
		if (rs.next()) {
			Student emp = new Student();
			
			emp.setStudentId(rs.getInt(1));
			emp.setStudentName(rs.getString(2));
			
			emp.setGender(rs.getString(3));
			emp.setEmailId(rs.getString(4));
			emp.setPassword(rs.getString(5));
			
			return emp;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return null;
}

public int deleteStudent(int studentId) {
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String deleteQuery = "delete from student where studentId=?";
	
	
	try {
		pst = con.prepareStatement(deleteQuery);
		pst.setInt(1, studentId);
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	return 0;
}
public int updateStudent(Student emp) {
	
	Connection con = DbConnection.getConnection();
	PreparedStatement pst = null;
	
	String updateQuery = "update student set studentName=?, gender=?, emailId=?, password=? where studentId=?";
	
	try {
		pst = con.prepareStatement(updateQuery);
		
		pst.setString(1, emp.getStudentName());
		
		pst.setString(2, emp.getGender());
		pst.setString(3, emp.getEmailId());
		pst.setString(4, emp.getPassword());
		pst.setInt(5, emp.getStudentId());
		
		return pst.executeUpdate();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	return 0;
}


}








	


