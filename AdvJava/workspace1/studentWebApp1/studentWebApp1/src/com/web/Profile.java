package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dto.Student;


@WebServlet("/Profile")
public class Profile extends HttpServlet {
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		Student emp = (Student) session.getAttribute("emp");
		
		//RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage");
		//rd.include(request, response);
		
		//OR
		
		request.getRequestDispatcher("StudentHomePage.jsp").include(request, response);
		
		out.println("<table border='2' align='center'>");

		out.println("<tr>");
		out.println("<th>StudentId</th>");
		out.println("<th>StudentName</th>");
		
		out.println("<th>Gender</th>");
		out.println("<th>EmailId</th>");
		out.println("<th>Password</th>");
		out.println("</tr>");

		out.println("<tr>");
		out.println("<td>" + emp.getStudentId()   + "</td>");
		out.println("<td>" + emp.getStudentName() + "</td>");
		
		out.println("<td>" + emp.getGender()  + "</td>");
		out.println("<td>" + emp.getEmailId() + "</td>");
		out.println("<td>" + emp.getPassword()+ "</td>");
		out.println("</tr>");

		out.println("</table>");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
