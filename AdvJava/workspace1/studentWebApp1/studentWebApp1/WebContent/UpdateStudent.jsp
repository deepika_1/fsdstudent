<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UpdateStudent</title>
</head>
<body>

	<jsp:include page="AdminHomePage.jsp" />
	
	
	<form action="UpdateStudent" method="post">

	<table align="center">
		<tr>
			<td>StudentId</td>
			<td><input type="text" name="studentId" value="${emp.studentId}" readonly/></td>
		</tr>
		<tr>
			<td>StudentName</td>
			<td><input type="text" name="studentName" value="${emp.studentName}" /></td>
		</tr>
		
		<tr>
			<td>Gender</td>
			<td>
				<select name="gender">
					<option value="${emp.gender}" selected>${emp.gender}</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
					<option value="Others">Others</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>EmailId</td>
			<td><input type="text" name="emailId" value="${emp.emailId}" readonly /></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" value="${emp.password}" readonly/></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button>Update Student</button>
			</td>
		</tr>
	</table>
</form>
	
</body>
</html>
