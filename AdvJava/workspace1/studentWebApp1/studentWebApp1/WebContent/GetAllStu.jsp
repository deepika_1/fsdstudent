<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.List, com.dto.Student"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllStudent</title>
</head>
<body>

	<jsp:include page="AdminHomePage.jsp" />

	<table border="2" align="center">

		<tr>
			<th>studentId</th>
			<th>studentName</th>
			
			<th>Gender</th>
			<th>EmailId</th>
			<th colspan="2">Actions</th>
		</tr>

		<c:forEach var="emp" items="${empList}">
		<tr>
			<td> ${stu.studentId}  </td>
			<td> ${stu.studentName}</td>
			
			<td> ${stu.gender} </td>
			<td> ${stu.emailId}</td>
			<td>Edit</td>
			<td><a href='EditStudent?studentId=${emp.studentId }'>Edit</a></td>
			<td> <a href='DeleteStudent?studentId=${emp.studentId}'>Delete</a> </td>
		</tr>
		</c:forEach>

	</table>

</body>
</html>
